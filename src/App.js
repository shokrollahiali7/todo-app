import Layout from "./containers/layout/Layout";
import Notification from "./containers/notification/Notification";
import Routes from "./Routes";

function App() {
  return (
    <>
      <Notification />
      <Layout>
        <Routes />
      </Layout>
    </>
  );
}

export default App;
