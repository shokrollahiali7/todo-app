import React, { Suspense } from "react";
import { Route, Switch, Redirect } from "react-router-dom";

const TodoList = React.lazy(() => import("./containers/todo/TodoList"));
const TodoEdit = React.lazy(() => import("./containers/todo/TodoEdit"));
const TodoAdd = React.lazy(() => import("./containers/todo/TodoAdd"));

const routes = [
  { component: <TodoList />, path: "/" },
  { component: <TodoAdd />, path: "/add" },
  { component: <TodoEdit />, path: "/edit/:id" },
];

const Routes = () => {
  return (
    <Suspense
      fallback={
        <div className="text-center">
          <p>صفحه در حال آماده شدن می باشد. لطفا کمی صبر کنید</p>
        </div>
      }
    >
      <Switch>
        {routes.map((route) => (
          <Route key={route.path} exact path={route.path}>
            {route.component}
          </Route>
        ))}
        <Route path="*">
          <Redirect to="/" />
        </Route>
      </Switch>
    </Suspense>
  );
};

export default Routes;
