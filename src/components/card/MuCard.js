import Card from "@material-ui/core/Card";

const MuCard = (props) => {
  const { className, children } = props;
  return <Card className={className}>{children}</Card>;
};

export default MuCard;
