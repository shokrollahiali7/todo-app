import TextField from "@material-ui/core/TextField";

const MuTextField = (props) => {
  const {
    value,
    label,
    defaultValue,
    helperText,
    id,
    variant,
    disabled,
    className,
    onChange,
    ...otherProps
  } = props;

  return (
    <TextField
      value={value}
      label={label}
      defaultValue={defaultValue}
      helperText={helperText}
      id={id}
      inputProps={{ style: { fontSize: "1.4rem" } }}
      InputLabelProps={{ style: { fontSize: "1.2rem" } }}
      variant={variant}
      disabled={disabled}
      className={className}
      onChange={onChange}
      {...otherProps}
    />
  );
};

export default MuTextField;
