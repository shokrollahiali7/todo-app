import CircularProgress from "@material-ui/core/CircularProgress";

const MuLoading = (props) => {
  return (
    <div className="Mu_loading">
      <CircularProgress />
    </div>
  );
};

export default MuLoading;
