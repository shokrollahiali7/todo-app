import Button from "@material-ui/core/Button";

const MuButton = (props) => {
  const {
    startIcon,
    endIcon,
    variant,
    color,
    size,
    className,
    onclick,
    children,
  } = props;

  return (
    <Button
      variant={variant}
      startIcon={startIcon}
      endIcon={endIcon}
      color={color}
      className={className}
      size={size}
      onclick={onclick}
    >
      {children}
    </Button>
  );
};

export default MuButton;
