import IconButton from "@material-ui/core/IconButton";

const MuIconButton = (props) => {
  const { disabled, className, children, onClick } = props;

  return (
    <IconButton disabled={disabled} className={className} onClick={onClick}>
      {children}
    </IconButton>
  );
};

export default MuIconButton;
