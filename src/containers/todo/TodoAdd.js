import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusSquare } from "@fortawesome/free-solid-svg-icons";

import MuTextField from "../../components/input/textField/MuTextField";
import MuIconButton from "../../components/button/MuIconButton";
import useInput from "../../hooks/use-input";
import { addTodoList } from "../../redux/actions/todo-action";
import { isInputValid } from "../../helpers/validationHelpers";
import { enumeration } from "../../configs/enumeration";
import { constant } from "../../configs/constants";

const TodoAdd = () => {
  const {
    value: todoValue,
    hasError: isTodoHasError,
    valueChangeHandler,
    inputBlurHandler,
  } = useInput("", isInputValid);

  const history = useHistory();

  const dispatch = useDispatch();

  const addTodoClicked = () => {
    dispatch(
      addTodoList(todoValue, () => {
        history.push("/");
      })
    );
  };
  const handleKeyDown = (e) => {
    if (e.keyCode === enumeration.keyboard.enter) addTodoClicked();
  };
  return (
    <div className="d-flex justify-content-between border border-3 rounded py-2">
      <MuTextField
        onKeyDown={handleKeyDown}
        error={isTodoHasError}
        fullWidth
        label="عنوان تسک"
        className="me-3"
        autoFocus
        value={todoValue}
        helperText={
          isTodoHasError &&
          `تعداد حروف باید بیشتر از ${constant.minNumberOfLetters} باشد`
        }
        onChange={valueChangeHandler}
        onBlur={inputBlurHandler}
      />
      <MuIconButton
        className="me-4"
        onClick={addTodoClicked}
        disabled={isTodoHasError}
      >
        <FontAwesomeIcon icon={faPlusSquare} />
      </MuIconButton>
    </div>
  );
};

export default TodoAdd;
