import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";

import MuCard from "../../components/card/MuCard";
import { viewLinks } from "../../configs/links";
import { isEmpty } from "../../helpers/validationHelpers";
import { getTodoList } from "../../redux/actions/todo-action";
import TodoItem from "./TodoItem";

let isInitial = true;

const TodoList = () => {
  const todoReducer = useSelector((state) => state.todoReducer);

  const dispatch = useDispatch();

  useEffect(() => {
    if (isInitial) {
      dispatch(getTodoList());
      isInitial = false;
    }
  }, [dispatch]);

  return (
    <MuCard className="todo p-5">
      <div className="text-center">
        <p className="mb-3 fw-bold font-size-large">لیست تسک ها</p>
      </div>
      <Link to={viewLinks.todoAdd} className="todo_add-btn">
        افزودن تسک
      </Link>
      <div className="text-center">
        {isEmpty(todoReducer.todos) && !todoReducer.isLoading ? (
          <p>هیچ تسکی وجود ندارد.</p>
        ) : (
          todoReducer.todos.map((todo) => (
            <TodoItem key={todo.id} todo={todo} />
          ))
        )}
      </div>
    </MuCard>
  );
};

export default TodoList;
