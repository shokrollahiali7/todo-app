import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { useParams } from "react-router-dom";
import { faEdit } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import MuIconButton from "../../components/button/MuIconButton";
import MuTextField from "../../components/input/textField/MuTextField";
import userInput from "../../hooks/use-input";
import { editTodoList } from "../../redux/actions/todo-action";
import { isInputValid } from "../../helpers/validationHelpers";
import { enumeration } from "../../configs/enumeration";
import { constant } from "../../configs/constants";

const TodoEdit = () => {
  const todoReducer = useSelector((state) => state.todoReducer);
  const id = useParams().id;
  const selectedTodo = todoReducer.todos.find((todo) => todo.id === id);
  const {
    value: todoValue,
    hasError: isTodoHasError,
    valueChangeHandler,
    inputBlurHandler,
  } = userInput(selectedTodo?.title, isInputValid);

  const history = useHistory();

  const dispatch = useDispatch();

  const editTodoClicked = () => {
    if (todoValue === selectedTodo?.title) {
      history.push("/");
      return;
    }
    dispatch(editTodoList(todoValue, id, () => history.push("/")));
  };
  const handleKeyDown = (e) => {
    if (e.keyCode === enumeration.keyboard.enter) editTodoClicked();
  };
  return (
    <div className="d-flex justify-content-between border border-3 rounded py-2">
      <MuTextField
        onKeyDown={handleKeyDown}
        error={isTodoHasError}
        fullWidth
        label="عنوان تسک"
        className="me-3"
        autoFocus
        value={todoValue}
        helperText={
          isTodoHasError &&
          `تعداد حروف باید بیشتر از ${constant.minNumberOfLetters} باشد`
        }
        onChange={valueChangeHandler}
        onBlur={inputBlurHandler}
      />
      <MuIconButton
        className=" me-4"
        onClick={editTodoClicked}
        disabled={isTodoHasError}
      >
        <FontAwesomeIcon icon={faEdit} />
      </MuIconButton>
    </div>
  );
};

export default TodoEdit;
