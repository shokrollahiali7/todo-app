import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { faEdit } from "@fortawesome/free-solid-svg-icons";

import MuCard from "../../components/card/MuCard";
import MuIconButton from "../../components/button/MuIconButton";
import { removeTodoList } from "../../redux/actions/todo-action";
import { viewLinks } from "../../configs/links";
import { stringHelper } from "../../helpers/commonHelper";

const TodoItem = (props) => {
  const { todo } = props;

  const dispatch = useDispatch();

  const deleteTodoHandler = () => {
    dispatch(removeTodoList(todo.id));
  };

  return (
    <MuCard className="todo_item">
      <p className="ms-auto">{todo.title}</p>
      <MuIconButton onClick={deleteTodoHandler}>
        <FontAwesomeIcon icon={faTrash} />
      </MuIconButton>
      <Link
        to={stringHelper(viewLinks.todoEdit, todo.id)}
        className="todo_edit"
      >
        <FontAwesomeIcon icon={faEdit} />
      </Link>
    </MuCard>
  );
};

export default TodoItem;
