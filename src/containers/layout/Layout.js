import Container from "@material-ui/core/Container";
import Header from "./Header";

const Layout = (props) => {
  return (
    <div className="layout">
      <Header />
      <main className="mt-5">
        <Container maxWidth="sm">{props.children}</Container>
      </main>
    </div>
  );
};

export default Layout;
