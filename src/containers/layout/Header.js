import { NavLink } from "react-router-dom";
import { viewLinks } from "../../configs/links";

const navLinks = [
  { title: "لیست تسک ها", path: viewLinks.todoList },
  { title: "اضافه کردن تسک ", path: viewLinks.todoAdd },
];

const Header = () => {
  return (
    <header className="header">
      <h1 className="ms-auto">مدیریت تسک ها</h1>
      <nav className="header_nav">
        <ul className="d-flex m-0 p-0">
          {navLinks.map((navLink) => (
            <li key={navLink.path}>
              <NavLink to={navLink.path} exact activeClassName="active">
                {navLink.title}
              </NavLink>
            </li>
          ))}
        </ul>
      </nav>
    </header>
  );
};

export default Header;
