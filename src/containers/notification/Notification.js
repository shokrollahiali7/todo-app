import { useSelector } from "react-redux";
import MuLoading from "../../components/loading/MuLoading";

const Notification = () => {
  const todoReducer = useSelector((state) => state.todoReducer);
  return (
    <>
      {todoReducer.isLoading && <MuLoading />}
      {todoReducer.error && (
        <section className="text-center">
          <p className="notification">
            {`${todoReducer.error} (!خطا!. برای اتصال به سرور Firebase نیاز به فیلترشکن است)`}
          </p>
        </section>
      )}
    </>
  );
};

export default Notification;
